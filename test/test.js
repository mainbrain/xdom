import '../dist/xdom.js';

const doc = document;
const $log = doc.qs('#log');
const log = msg => { $log.innerHTML += `${msg}<br>` };

const $x = doc.qs('x');

const tests = () => {

   // $x.css({ ps: 'absolute', w: '20px', h: '20px', bg: '#f00', o: .5, l: 0, t: '10%', mT: '20px', tf: ['tX:10px', 'tX:10%'], ts: { o: '.25s ease-in-out' } });
   $x.css({ ps: 'absolute', w: '20px', h: '20px', bg: '#f00', l: 0, t: '10%', mT: '20px', tf: ['tX:10px', 'tX:10%'] });
   log(`x.style = "${$x.style.cssText}"`);
   
   window.on('load', anim, { once: true });
}

const anim = () => {
   $x.css({ bg: '#f00', o: .5, l: 0, t: '10%', mT: '20px', tf: ['tX:10px'], ts: { o: '1s ease-in-out' } });
   log(`x.css('o') = ${$x.css('o')}`);
   log(`x.style = "${$x.style.cssText}"`);
}

if (xDOM) {
   $log.innerHTML = '';
   log(`xDOM ${xDOM.v}`);
   tests();
} else {
   console.error('no xDOM found.');
}