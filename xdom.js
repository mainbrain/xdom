/*
* MainBrain, Lda. - eXtended DOM - A DOM extension library.
*/

// interface transformAxes {
//    axes: string[]
// }

class transformAxes {
   name;

   constructor(name) {
      this.name = name;
   }

   set(n, map) {
      for (let s of this.axes)
         map.set(`${n}${s}`, `${this.name}${s}`);
   }
}

transformAxes.prototype.axes = ['X', 'Y', 'Z', '3d'];

class transformMin {
   map; //: Map<string, string | transformAxes>

   constructor(data) {
      let map = this.map = new Map(Object.entries(data));

      for (let [n, v] of Object.entries(data)) {
         if (v.constructor == transformAxes) {
            map.set(n, v.name);
            v.set(n, map);
         }
      }
   }

   /**
    * Method to translate transformMin String[] to proper CSS transform syntax.
    * @param {String[]} data Needs to be array as we can have ['tX:10px','tX:10%']
    * @returns {String} Returns list prop value 'translateX(10px)'
    */
   toCSS(data) {
      let value = '';
      for (let e of data) {
         const s = e.split(':');
         if (s.length === 2)
            value += `${value.length ? ' ' : ''}${this.map.get(s[0]) || s[0]}(${s[1]})`;
         else
            throw `transformMin invalid entry ${e}`;
      }
      return value;
   }
}

// For optimization and coherence min alias cannot have len > 3.

const tfMin = new transformMin({
   t: new transformAxes("translate"),
   s: new transformAxes("scale"),
   r: new transformAxes("rotate"),
   p: "perspective",
   m: "matrix",
   m3d: "matrix3d",
   sk: "skew",
   skX: "skewX",
   skY: "skewY"
});

//console.log('tfMin', tfMin);

// interface propertySides {
//    sides: string[];
// }

class propertySides {
   name; //: string;

   constructor(name) {
      this.name = name;
   }

   set(n, map) {
      for (let s of this.sides)
         map.set(`${n}${s.toUpperCase()}`, `${this.name}-${map.get(s)}`);
   }
}

propertySides.prototype.sides = ['t', 'r', 'b', 'l'];

// Properties minification dictionary.
// TODOs:
// - Possible optimization of index's based on length, propMin[propName.length].get(propName), so we would have propMin[1] for length 1 props like "o" and "w", and propMin[2] for 2 length props like "mt" and "tf".

class propertyMin {
   map; //: Map<string, string | propertySides>

   constructor(data) {
      let map = this.map = new Map(Object.entries(data));

      for (let [n, v] of Object.entries(data)) {
         if (v.constructor == propertySides) {
            map.set(n, v.name);
            v.set(n, map);
         }
      }
   }

   get(name) {
      return this.map.get(name);
   }
}

// For optimization and coherence min alias cannot have len > 3.

const propMin = new propertyMin({
   o: "opacity",
   w: "width",
   h: "height",
   l: "left",
   r: "right",
   t: "top",
   b: "bottom",
   m: new propertySides("margin"),
   p: new propertySides("padding"),
   ps: "position",
   tf: "transform",
   ts: "transition",
   bg: "background",
   bgc: "background-color",
   bgi: "background-image",
   bd: new propertySides("border"),
});

//console.log('propMin', propMin);

const transition2CSS = (data) => {
   let value = '';
   for (let [p, v] of Object.entries(data)) {
      if (p.length < 4)
         p = propMin.get(p) || p;
      value += `${value.length ? ', ' : ''}${p} ${v}`;
   }
   return value;
}

/**
 * Extension function to get or set Element inline CSS style values.
 * Get:
 * @param {String} css Will get value of inline style property.
 * @returns {?String} Inline style property value, empty {String} ("") if style has no inline value or {undefined} if invalid style property.
 * Set:
 * @param {Object} css Will map (property, value) to apply inline style.
 * @returns {Element|HTMLCollection} Returns caller so it can be chained.
 */
function css(css) {
   if (String.isString(css)) {
      let p = css;
      if (p.length < 4)
         p = propMin.get(p) || p;
      return this.style[p];
   }
   if (this.length) {
      for (let i = this.length - 1; i >= 0; i--) {
         this.css.call(this[i], css);
      }
   } else {
      for (let p in css) {
         let v = css[p];
         if (p.length < 4)
            p = propMin.get(p) || p;
         if (p === "transform" && v.constructor === Array)
            this.style[p] = tfMin.toCSS(v);
         else if (p === "transition" && v.constructor === Object)
            this.style[p] = transition2CSS(v);
         else
            this.style[p] = v;
      }
   }
   return this;
}

/**
 * Extension function to get or set {Element} attributes values.
 * Get:
 * @param {String} att Will get value of attribute.
 * @returns {?String} Attribute value or {Null} if attribute not found. If attribute has no value it will return empty {String} ("").
 * Set:
 * @param {Object} att Will map (property, value) to apply attributes.
 * @returns {Element|HTMLCollection} Returns caller so it can be chained.
 */
function att(att) {
   if (String.isString(att)) {
      let p = att;
      if (p.length < 4)
         p = propMin.get(p) || p;
      return this.getAttribute(p);
   }
   if (this.length) {
      for (let i = this.length - 1; i >= 0; i--) {
         this.att.call(this[i], att);
      }
   } else {
      for (let p in att) {
         let v = att[p];
         if (p.length < 4)
            p = propMin.get(p) || p;
         this.setAttribute(p, v);
      }
   }
   return this;
}

/**
 * Alias extension of querySelector() for simplified use.
 * @param {String} sel CSS Selector to search for.
 * @returns {?Element} Will return first {Element} found only or {Null} if none is found.
 */
function qs(sel) {
   return this.querySelector(sel);
}

/**
 * Alias extension of querySelectorAll() for simplified use.
 * @param {String} sel CSS Selector to search for.
 * @returns {?HTMLCollection} Will return {HTMLCollection} for single, multiple or empty if none is found.
 */
function qsa(sel) {
   return this.querySelectorAll(sel);
}

/**
 * DOM Extensions.
 */
// interface Element {
//    att(values: string | Object): Element | String | undefined
//    qs(selector: string): Element | undefined
//    qsa(selector: string): NodeList
// }

// interface Document {
//    qs(selector: string): Element | undefined
//    qsa(selector: string): NodeList
// }

// interface HTMLElement {
//    css(values: string | Object): HTMLElement | String | undefined
// }
// interface SVGElement {
//    css(values: string | Object): SVGElement | String | undefined
// }
// interface HTMLCollection {
//    css(values: string | Object): HTMLCollection | String | undefined
//    att(values: string | Object): HTMLCollection | String | undefined
// }

// // For SVG qsa() returns
// interface NodeList {
//    css(values: string | Object): NodeList | String | undefined
//    att(values: string | Object): NodeList | String | undefined
// }

// interface EventTarget {
//    on(type: string, listener: EventListenerOrEventListenerObject, options?: boolean | AddEventListenerOptions): void
//    off(type: string, callback: EventListenerOrEventListenerObject, options?: boolean | EventListenerOptions): void
// }

Element.prototype.att = att;
HTMLElement.prototype.css = css;
SVGElement.prototype.css = css;
HTMLCollection.prototype.css = css;
HTMLCollection.prototype.att = att;
NodeList.prototype.css = css;
NodeList.prototype.att = att;

Element.prototype.qs = Element.prototype.querySelector;
Element.prototype.qsa = Element.prototype.querySelectorAll;
Document.prototype.qs = Document.prototype.querySelector;
Document.prototype.qsa = Document.prototype.querySelectorAll;
EventTarget.prototype.on = EventTarget.prototype.addEventListener;
EventTarget.prototype.off = EventTarget.prototype.removeEventListener;

/**
 * JavaScript Extensions.
 */

// interface StringConstructor {
//    isString(value: any): boolean;
// }
String.isString = function (val) {
   return val.substr ? true : false;
}

// interface Math {
//    clamp(num: number, min: number, max: number): number;
// }

if (Math.clamp == null) // Math.clamp is being proposed for ECMA.
   Math.clamp = (num, min, max) => Math.min(Math.max(num, min), max);

// interface Window {
//    xDOM: object
// }

Window.prototype.xDOM = { v: '1.0.0' }

export { tfMin as transformMin, propMin as propertyMin };